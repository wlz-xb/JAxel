/**
 *Copyright 2013 by dragon.
 *
 *File name: Piece.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-9 下午6:07:05
 *Description: 
 */
package com.dragon.jaxel.bittorrent;

/**
 * @author dragon8
 * 
 */
public class Piece {

	final int pieceIndex;

	final static int CHUNK_SIZE = 16 * 1024;
	/**
	 * 将一个大片段分割为小片段
	 */
	transient LittlePiece[] littlePieces;

	byte[] buffer = null;

	/**
	 * 
	 */
	public Piece(int pieceIndex) {
		// TODO Auto-generated constructor stub
		this.pieceIndex = pieceIndex;
	}

	void init(long fileOffset, int pieceLen) {
		int count = pieceLen / CHUNK_SIZE;
		littlePieces = new LittlePiece[count];
		int pieceOffset = 0;
		for (int i = 0; i < count; i++) {
			littlePieces[i] = new LittlePiece(i, pieceOffset, fileOffset
					+ pieceOffset, CHUNK_SIZE);
			pieceOffset += CHUNK_SIZE;
		}
	}

	boolean isFinished() {
		for (int i = 0; i < littlePieces.length; i++) {
			if (!littlePieces[i].isFinished()) {
				return false;
			}
		}
		return true;
	}
}
