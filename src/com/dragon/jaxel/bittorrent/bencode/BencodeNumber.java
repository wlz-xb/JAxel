/**
 *Copyright 2013 by dragon.
 *
 *File name: BencodeNumber.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-8 上午9:46:48
 *Description: 
 */
package com.dragon.jaxel.bittorrent.bencode;

/**
 * Bencode 整型类
 * 
 * @author dragon8
 * 
 */
public class BencodeNumber extends BencodeValue {

	long val;

	/**
	 * @param bytes
	 */
	public BencodeNumber(byte[] bytes) {
		super(bytes);
		// TODO Auto-generated constructor stub
		val = Long.valueOf(new String(bytes));
	}

	public long getData() {
		return val;
	}
}
