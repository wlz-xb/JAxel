/**
 *Copyright 2013 by dragon.
 *
 *File name: BnecodeList.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-8 上午9:50:23
 *Description: 
 */
package com.dragon.jaxel.bittorrent.bencode;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Bnecode序列
 * 
 * @author dragon8
 * 
 */
public class BencodeList extends BencodeValue {

	ArrayList<BencodeValue> list = new ArrayList<BencodeValue>();

	public BencodeList() {
		super(null);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dragon.bittorrent.bencode.BencodeValue#getBytes()
	 */
	@Override
	public byte[] getBytes() {
		// TODO Auto-generated method stub
		try {
			return Bencode.encode(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public BencodeValue get(int index) {
		return list.get(index);
	}

	public void add(BencodeValue bencodeValue) {
		list.add(bencodeValue);
	}

	public int size() {
		return list.size();
	}

	public ArrayList<BencodeValue> getList() {
		return list;
	}
}
