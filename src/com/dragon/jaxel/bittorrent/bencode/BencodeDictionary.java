/**
 *Copyright 2013 by dragon.
 *
 *File name: BencodeDictionary.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-8 上午9:58:23
 *Description: 
 */
package com.dragon.jaxel.bittorrent.bencode;

import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;

/**
 * @author dragon8
 * 
 */
public class BencodeDictionary extends BencodeValue {

	Map<String, BencodeValue> dictionary = new TreeMap<String, BencodeValue>();

	/**
	 * Bencode字典
	 */
	public BencodeDictionary() {
		super(null);
		// TODO Auto-generated constructor stub
	}

	public void put(String key, BencodeValue value) {
		dictionary.put(key, value);
	}

	public BencodeValue get(String key) {
		return dictionary.get(key);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.dragon.bittorrent.bencode.BencodeValue#getBytes()
	 */
	@Override
	public byte[] getBytes() {
		// TODO Auto-generated method stub
		try {
			return Bencode.encode(this);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return null;
		}
	}

	public Map<String, BencodeValue> getMap() {
		return dictionary;
	}
}
