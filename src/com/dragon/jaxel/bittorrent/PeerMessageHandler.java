/**
 *Copyright 2013 by dragon.
 *
 *File name: PeerMessageHandler.java
 *Author:      dragon
 *Email:       fufulove2012@gmail.com
 *Blog:        http://blog.csdn.net/xidomlove
 *Version:     1.0.0
 *Date:        2013-10-9 下午10:59:48
 *Description: 
 */
package com.dragon.jaxel.bittorrent;

import java.nio.ByteBuffer;

/**
 * @author dragon8
 * 
 */
public interface PeerMessageHandler {
	public void handleMessage(ByteBuffer buffer, int messageLength);
}
